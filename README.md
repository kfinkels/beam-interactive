# Interactive Beam

This was copied from 
[Apache Beam: Interactive Beam](https://github.com/apache/beam/blob/master/sdks/python/apache_beam/runners/interactive) for a demo lesson

## Getting Started

* clone this repository

### Pre-requisites

*   Install [Python 3.7 or newer](https://www.python.org/downloads/) with your favorite
    system package manager.

*   Make sure you have [Virtualenv](https://virtualenv.pypa.io/en/latest/installation.html)
    
*   Install [GraphViz](https://www.graphviz.org/download/) with your favorite
    system package manager.

-   Install [JupyterLab](https://jupyter.org/install.html). 

    ```bash
    pip3 install jupyterlab
    ```

-   Install, create and activate your [venv](https://docs.python.org/3/library/venv.html).
    (optional but recommended)

    ```bash
    cd beam-interactive
    virtualenv -p python3 venv
    source venv/bin/activate
    ```

-   Install Apache Beam and Beam Interactive for the virtual environment.
    
    ```bash
    pip3 install apache-beam
    pip3 install "apache-beam[interactive]"
    ```

-   Install an IPython kernel for the virtual environment.

    ```bash
    pip3 install ipykernel
    python -m ipykernel install --user --name beam_venv_kernel --display-name "Python3 (beam_venv)"
    ```

    **CHECK** that IPython kernel `beam_venv_kernel` is available for Jupyter to
    use.

    ```bash
    jupyter kernelspec list
    ```

-   Extend JupyterLab through labextension.

    All jupyter labextensions need nodejs

    ```bash
    brew install node
    ```

    Enable ipywidgets

    ```bash
    pip3 install ipywidgets
    jupyter labextension install @jupyter-widgets/jupyterlab-manager
    ```

### Start the notebook

To start the notebook, simply run

```bash
jupyter lab
```

Optionally increase the iopub broadcast data rate limit of jupyterlab

```bash
jupyter lab --NotebookApp.iopub_data_rate_limit=10000000
```


This automatically opens your default web browser pointing to
http://localhost:8888/lab.

You can create a new notebook file by clicking `Python3 (beam_venv)` from the launcher
page of jupyterlab.

Or after you've already opened a notebook, change the kernel by clicking
`Kernel` > `Change Kernel` > `Python3 (beam_venv)`.

Voila! You can now run Beam pipelines interactively in your Jupyter notebook!

In the notebook, you can use `tab` key on the keyboard for auto-completion.
To turn on greedy auto-completion, you can run such ipython magic

```
%config IPCompleter.greedy=True
```

You can also use `shift` + `tab` keys on the keyboard for a popup of docstrings at the
current cursor position.

## More Information

*   [Apache Beam Python SDK Quickstart](https://beam.apache.org/get-started/quickstart-py/)
